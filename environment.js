define([
	'require',
	'madjoh_modules/json_url/json_url',
	'madjoh_modules/session/session'
],
function (require, JSON_URL, Session){
	var Environment = {
		dev_ids : [],
		getEnv : function(){
			if(document.environment) return document.environment;

			var url = document.location.href;
			var json = JSON_URL.toJSON(url);

			var env = null;
			if(document.location.hostname === 'localhost' && !window.cordova && json.env === 'local') env = 'local';
			else if(json.env === 'test') env = 'test';
			else env = 'prod';

			document.environment = env;

			return env;
		},
		switchEnvironment : function(){
			Session.close();

			var env = Environment.getEnv();
			if(env === 'prod') document.environment = 'test';
			else document.environment = 'prod';

			Environment.init();
		}
	};

	return Environment;
});